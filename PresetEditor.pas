uses GraphABC;

type
  field = array[0..601,0..351] of boolean;

const
  maxWidth = 1920;
  maxHeigth = 1080;

var
  editCheck:boolean;
  stages,delay:integer;
  xCells,yCells,cellSize:integer;
  editRun:boolean;
  torusAnswer,gridAnswer,drawAnswer,editAnswer:string;
  oldPresetName,newPresetName:string;
  startPosition:field;

procedure readFileNames;
begin
  write('Preset name: ');
  read(oldPresetName); writeln(oldPresetName);
  oldPresetName += '.life';
  
  write('New preset name: ');
  readln;
  read(newPresetName); writeln(newPresetName); 
  newPresetName += '.life';
  writeln(newPresetName,' will be created.');
  
  write('Do you want to edit settings (or just edit cells)?   ');
  readln;
  readln(editAnswer); 
  editRun := editAnswer = 'yes';
  writeln(editAnswer);
end;

//grab preset settings from 'life' file
procedure grabSettings;
var
  i,j:integer;
  number:integer;
  preset:text;
begin
  assign(preset,oldPresetName);
  reset(preset);
  readln(preset,xCells);
  readln(preset,yCells);
  readln(preset,cellSize);
  readln(preset,stages);
  readln(preset,delay);
  readln(preset,torusAnswer);
  readln(preset,gridAnswer);
  readln(preset,drawAnswer);
  
  for j:=1 to yCells do
  begin
    for i:=1 to xCells do
    begin
      read(preset,number);
      startPosition[i,j] := (number = 1);
    end;
    readln(preset);
  end;
  close(preset);
end;

//reads preset settings from screen
procedure editSettings;
begin
  write('Number of cells in a row (was ',xCells,'): ');
  read(xCells); writeln(xCells);
  write('Number of cells in a column (was ',yCells,'): ');
  read(yCells); writeln(yCells);
  write('Size of cells, pixels (was  ',cellSize,'): ');
  read(cellSize); writeln(cellSize);
  write('Number of stages will be run (0 — eternal simulation) (was ',stages,')? ');
  read(stages); writeln(stages);
  write('Delay, ms  (was ',delay,'): ');
  read(delay); writeln(delay);
  write('Use torus surface (last cells border with first ones) (was  ',torusAnswer,')? ');
  readln;
  readln(torusAnswer); 
  writeln(torusAnswer);
  write('Draw grid  (was  ',gridAnswer,')?  ');
  readln(gridAnswer);
  writeln(gridAnswer);
  write('Draw every stage (or just last one) (was ',drawAnswer,')? ');
  readln(drawAnswer);
  writeln(drawAnswer);
end;

//resizes window
procedure resizeWindow;
var
  width,heigth:integer;
begin
  width := (cellSize+1)*(xCells+2)+3;
  heigth := (cellSize+1)*(yCells+2)+3;
  initWindow(trunc((maxWidth-width)/2),trunc((maxHeigth-heigth)/2),width,heigth);
end;

//draws grid
procedure Grid;
var
  i:integer;
begin
  for i:=1 to xCells+1 do
    Line((cellSize+1)*(i-1)+1,1,(cellSize+1)*(i-1)+1,(cellSize+1)*yCells+1);
  for i:=1 to yCells+1 do
    Line(1,(cellSize+1)*(i-1)+1,(cellSize+1)*xCells+1,(cellSize+1)*(i-1)+1);
end;

//draws button for quick cells adding
procedure Buttons;
var
  i:integer;
begin
  for i:=xCells+2 to xCells+3 do
    Line((cellSize+1)*(i-1)+1,1,(cellSize+1)*(i-1)+1,(cellSize+1)*yCells+1);
  for i:=1 to xCells+3 do
    Line((cellSize+1)*(i-1)+1,(yCells+1)*(cellSize+1)+1,(cellSize+1)*(i-1)+1,(yCells+2)*(cellSize+1)+1);
  for i:=yCells+2 to yCells+3 do
    Line(1,(cellSize+1)*(i-1)+1,(cellSize+1)*xCells+1,(cellSize+1)*(i-1)+1);
  for i:=1 to yCells+3 do
    Line((xCells+1)*(cellSize+1)+1,(cellSize+1)*(i-1)+1,(xCells+2)*(cellSize+1),(cellSize+1)*(i-1)+1);
end;

//adds cells
procedure addLife(x,y:integer);
begin
  setbrushcolor(clBlack);
  rectangle((cellSize+1)*(x-1)+1,(cellSize+1)*(y-1)+1,(cellSize+1)*x+2,(cellSize+1)*y+2);
  setbrushcolor(clWhite);
  startPosition[x,y]:=true;
end;

//removes cells
procedure removeLife(x,y:integer);
begin
  rectangle((cellSize+1)*(x-1)+1,(cellSize+1)*(y-1)+1,(cellSize+1)*x+2,(cellSize+1)*y+2);
  startPosition[x,y]:=false;
end;

//draws start position
procedure doStartPosition;
var
  i,j:integer;
begin
  for j:=1 to yCells do
    for i:=1 to xCells do
      if startPosition[i,j] = true
      then addLife(i,j);
end;

//edits current cell
procedure editCell(x,y,mb:integer); forward;

//turn cells editing on
procedure ableEdit(x,y,mb: integer);
begin
  editCheck:=true;
  editCell(x,y,mb);
end;

//turn cells editing off
procedure enableEdit(x,y,mb: integer);
begin
  editCheck:=false;
  editCell(x,y,mb);
end;

procedure editCell(x,y,mb: integer);
var
  i,j,getX,getY:integer;
begin
  if editCheck and (x>0) and (y>0)
  then
  begin
    GetX:=x;
    GetY:=y;

    x:=((getX-1) div (cellSize+1)) + 1;
    y:=((getY-1) div (cellSize+1)) + 1;
    
    if (x<=xCells) and (y<=yCells)
    then
      if mb = 1
      then
        addLife(x,y)
      else
        removeLife(x,y);
    
    if (x<=xCells) and (y=yCells+2)
    then
      if mb = 1
      then
        for i:=1 to yCells do
          addLife(x,i)
      else
        for i:=1 to yCells do
          removeLife(x,i);
    
    if (y<=yCells) and (x=xCells+2)
    then
      if mb = 1
      then
        for i:=1 to xCells do
          addLife(i,y)
      else
        for i:=1 to xCells do
          removeLife(i,y);
    
    if (x=xCells+2) and (y=yCells+2)
    then
      if mb = 1
      then 
        for j:=1 to yCells do
          for i:=1 to xCells do
            addLife(i,j)
      else
        for j:=1 to yCells do
          for i:=1 to xCells do
            removeLife(i,j);
  end;
end;

//records preset settings
procedure recordData;
var
  i,j:integer;
  newPreset:text;
begin
  assign(newPreset,newPresetName);
  rewrite(newPreset);
  
  writeln(newPreset,xCells);
  writeln(newPreset,yCells);
  writeln(newPreset,cellSize);
  writeln(newPreset,stages);
  writeln(newPreset,delay);
  writeln(newPreset,torusAnswer);
  writeln(newPreset,gridAnswer);
  writeln(newPreset,drawAnswer);
  
  for j:=1 to yCells do
  begin
    for i:=1 to xCells do
    begin
      if startPosition[i,j]
      then write(newPreset,'1 ')
      else write(newPreset,'0 ');
    end;
    writeln(newPreset);
  end;
  close(newPreset);
end;

begin
  initWindow(trunc(maxWidth/2)-250,trunc(maxHeigth/2)-200,500,300);  
  readFileNames;
  grabSettings;
  if editRun
  then editSettings;
  sleep(2000);

  setbrushcolor(clWhite);
  FillRectangle(0,0,500,200);
  setbrushcolor(clBlack);

  resizeWindow;
  Grid;
  Buttons;
  doStartPosition;

  OnMouseMove := editCell;
  OnMouseDown := ableEdit;
  OnMouseUp := enableEdit;
  OnClose := recordData;
end.
