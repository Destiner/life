uses GraphABC;

type
  field = array[0..2000,0..1100] of boolean;

var
  maxWidth,maxHeigth:integer;
  i,stages:integer;
  xCells,yCells,cellSize:integer;
  time,delay:integer;
  coefficient:real;
  torusAnswer,gridAnswer,drawAnswer:string[10];
  torusRun,gridRun,drawRun,presetRun:boolean;  
  oldStage,newStage:field;

//reads screen resolution from config file
procedure configScan;
var 
  config:text;
begin
  assign(config,'config.ini');
  reset(config);
  readln(config,maxWidth);
  readln(config,maxHeigth);
  close(config);
end;

//reads preset settings from 'life' file
procedure settingsScan;
var 
  i,j,life:integer;
  fileName:string;
  config:text;
begin
  write('Preset name: ');
  readln(fileName); writeln(fileName);
  fileName := 'Presets\' + fileName + '.life';
  
  while not(FileExists(fileName)) do
  begin
    write('Preset not found. Preset name: ');
    readln(fileName); writeln(fileName);
    fileName := 'Presets\' + fileName + '.life';
  end;
  
  assign(config,fileName);
  reset(config);
  readln(config,xCells);
  readln(config,yCells);
  readln(config,cellSize);
  readln(config,stages);
  readln(config,delay);
  readln(config,torusAnswer);
  readln(config,gridAnswer);
  readln(config,drawAnswer);
  
  if fileName = 'Presets\random.life'
  then 
  begin
    presetRun:=false;
    readln(config,coefficient);
  end  
  else
  begin
    presetRun:=true;
    for j:=1 to yCells do
    begin
      for i:=1 to xCells do
      begin
        read(config,life);
        oldStage[i,j]:=life=1;
      end;
      readln(config);
    end;
  end;
  
  sleep(1000);
  fillRectangle(0,0,500,25);
  close(config);
end;

procedure resizeWindow;
var
  width,heigth:integer;
begin
  if gridRun
  then
  begin
    width := (cellSize+1)*xCells+3;
    heigth := (cellSize+1)*yCells+3;
  end
  else
  begin
    width := (cellSize)*xCells+3;
    heigth := (cellSize)*yCells+3;
  end;
  initWindow(trunc((maxWidth-width)/2),trunc((maxHeigth-heigth)/2),width,heigth);
end;

procedure drawGrid;
var
  i:integer;
begin
  for i:=1 to xCells+1 do
  begin
    line((cellSize+1)*(i-1)+1,1,(cellSize+1)*(i-1)+1,(cellSize+1)*yCells+1);
  end;
  for i:=1 to yCells+1 do
  begin
    line(1,(cellSize+1)*(i-1)+1,(cellSize+1)*xCells+1,(cellSize+1)*(i-1)+1);
  end;
end;

//adds cells on screen
procedure addLife(x,y:integer);
begin
  setbrushcolor(clBlack);
  if gridRun
  then fillrectangle((cellSize+1)*(x-1)+2,(cellSize+1)*(y-1)+2,(cellSize+1)*x+1,(cellSize+1)*y+1)
  else fillrectangle(cellSize*(x-1)+1,cellSize*(y-1)+1,cellSize*x+1,cellSize*y+1);
  setbrushcolor(clWhite);
end;

//removes cells from screen
procedure removeLife(x,y:integer);
begin
  if gridRun
  then fillrectangle((cellSize+1)*(x-1)+2,(cellSize+1)*(y-1)+2,(cellSize+1)*x+1,(cellSize+1)*y+1)
  else fillrectangle(cellSize*(x-1)+1,cellSize*(y-1)+1,cellSize*x+1,cellSize*y+1);
end;

//helps connecting border cells
function torus (x,maxCells:integer):integer;
begin
  torus:=x;
  if torusRun
  then
  begin
    if x<1
    then torus:=maxCells;
    if x>maxCells
    then torus:=1;
  end;
end;

//counts neighbor cells
function lifeCount(x,y:integer):integer;
var
  counter:integer;
begin
  if oldStage[torus(x-1,xCells),y]
  then inc(counter);
  if oldStage[torus(x+1,xCells),y]
  then inc(counter);
  if oldStage[x,torus(y-1,yCells)]
  then inc(counter);
  if oldStage[x,torus(y+1,yCells)]
  then inc(counter);
  if oldStage[torus(x-1,xCells),torus(y-1,yCells)]
  then inc(counter);
  if oldStage[torus(x+1,xCells),torus(y+1,yCells)]
  then inc(counter);
  if oldStage[torus(x+1,xCells),torus(y-1,yCells)]
  then inc(counter);
  if oldStage[torus(x-1,xCells),torus(y+1,yCells)]
  then inc(counter);
  lifeCount:=counter;
end;

//draws start position
procedure startPosition;
var
  i,j:integer;
begin
  for i:=1 to xCells do
  begin
    for j:=1 to yCells do
    begin
      if oldStage[i,j]
      then addLife(i,j);
    end;
  end;
end;

//generates start position (using 'random' preset)
procedure generateStartPosition;
var
  i,j:integer;
begin
  randomize;
  for i:=1 to xCells do
  begin
    for j:=1 to yCells do
    begin
      if random<coefficient
      then 
      begin
        oldStage[i,j]:=true;
      end;
    end;
  end;
end;

//calculates stages using rules
procedure life; 
var
  i,j:integer;
begin
  for i:=1 to xCells do
  begin
    for j:=1 to yCells do
    begin
      if oldStage[i,j]
      then newStage[i,j]:=(lifeCount(i,j)=2) or (lifeCount(i,j)=3)
      else newStage[i,j]:=lifeCount(i,j)=3;
    end;
  end;
end;

//draws stages
procedure drawStage;
var
  i,j:integer;
begin
  if drawRun
  then
  begin
    for i:=1 to xCells do
    begin
      for j:=1 to yCells do
      begin
        if oldStage[i,j]
        then 
        begin
          if not(newStage[i,j])
          then removeLife(i,j);
        end
        else
        begin
          if newStage[i,j]
          then addLife(i,j);
        end;
      end;
    end;    
  end
  else
  begin
    for i:=1 to xCells do
    begin
      for j:=1 to yCells do
      begin
        if newStage[i,j]
        then addLife(i,j);
      end;
    end;
  end;
end;

//draws stages and delays them
procedure metaLife;
begin
  time := milliseconds;
  Life;
  if drawRun
  then drawStage;
  oldStage:=newStage;
  time := milliseconds - time;
  if (delay>time) and (drawRun)
  then sleep(delay-time);
end;

begin
  configScan;
  initWindow(trunc(maxWidth/2)-250,trunc(maxHeigth/2)-200,500,200);
  settingsScan;
  torusRun:=torusAnswer='yes';
  gridRun:=gridAnswer='yes';
  drawRun:=drawAnswer='yes';
  resizeWindow;

  if not(presetRun)
  then generateStartPosition;
    
  if drawRun
  then
  begin
    if gridRun
    then drawGrid;
    startPosition;
  end
  else writeln('Working…');

  if stages = 0
  then while true do metaLife
  else for i:=1 to stages do metaLife;
  
  if not(drawRun)
  then 
  begin
    fillrectangle(0,0,100,50);
    if gridRun
    then drawGrid;
    drawStage;
  end;
end.
